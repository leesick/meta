<?php

use kartik\file\FileInput;

echo FileInput::widget([
    'model' => $model,
    'attribute' => 'file',
    'pluginOptions' => [
        'uploadUrl' => "/site/upload",
        'enableResumableUpload' => true,
        'initialPreviewAsData' => true,
        'maxFileCount' => 5,
        'resumableUploadOptions' => [
            'chunkSize' => 1000, //Kb
            'maxRetries' => 100
        ]
    ],
]);
?>


