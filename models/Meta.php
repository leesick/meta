<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\Url;

/**
 * Class Meta
 * @package models
 */
class Meta extends Model
{
    public $id;
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'safe'],
        ];
    }

    public function upload($post, $files) {
        $targetDir = 'uploads/meta';
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }
        $fileBlob = 'fileBlob';
        if (isset($files[$fileBlob])) {

            $file = $files[$fileBlob]['tmp_name'];
            $fileName = $post['fileName'];
            $fileSize = $post['fileSize'];
            $fileId = $post['fileId'];
            $index = $post['chunkIndex'];
            $totalChunks = $post['chunkCount'];
            $targetFile = $targetDir . '/' . $fileName;
            if ($totalChunks > 1) {
                $targetFile .= '_' . str_pad($index, 4, '0', STR_PAD_LEFT);
            }
            if (move_uploaded_file($file, $targetFile)) {
                $chunks = glob("{$targetDir}/{$fileName}_*");
                $allChunksUploaded = $totalChunks > 1 && count($chunks) == $totalChunks;
                if ($allChunksUploaded) {
                    $outFile = $targetDir . '/' . $fileName;
                    $this->combineChunks($chunks, $outFile);
                }
                $zoomUrl = 'uploads/meta/' . $fileName;
                return [
                    'chunkIndex' => $index,
                    'initialPreviewConfig' => [
                        [
                            'type' => 'image',
                            'caption' => $fileName,
                            'key' => $fileId,
                            'fileId' => $fileId,
                            'size' => $fileSize,
                            'zoomData' => $zoomUrl,
                        ]
                    ],
                    'append' => true
                ];
            } else {
                return [
                    'error' => 'Error uploading chunk ' . $post['chunkIndex']
                ];
            }
        }
        return [
            'error' => 'No file found'
        ];
    }

    public function combineChunks($chunks, $targetFile)
    {
        $handle = fopen($targetFile, 'a+');

        foreach ($chunks as $file) {
            fwrite($handle, file_get_contents($file));
        }

        foreach ($chunks as $file) {
            @unlink($file);
        }

        fclose($handle);
    }



}
